from typing import List, Any, Union
my_list5 = [2, 10, 20, 21, 23, 24, 40, 55, 60, 61]
my_list4 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
my_list3 = [2, 4, 6, 8, 10, 12, 14]
my_list2 = []
my_list = [7, 4, 2, 7, 3, 4, 6, 7, 8, 9, 7, 0, 10, 7, 0, 1, 7, 6, 5, 7, 3, 2, 7, 9, 9, 8,7]
list_total = 0
n2 = 0
n3 = 0
n7 = 0
print(my_list)
for i in range(3, 17):
    list_total += my_list[i]
print("the sum of 3-16 is", list_total)
list_total = 0
for i in range(2, 10):
    list_total += my_list[i]
print("the sum of 2-9 is", list_total)
for item in my_list:
    if item == 2:
        n2 += 1
print("# of 2s =", n2)
for item in my_list:
    if item == 3:
        n3 += 1
print("# of 3s =", n3)
for item in my_list:
    if item == 7:
        n7 += 1
print("# of 7s =", n7)
for item in my_list:
    if item != 7:
        my_list2.append(item)
print("new list with all 7s removed =", my_list2)
n7 = 0
for item in my_list2:
    if item == 7:
        n7 += 1
print("# of 7s =", n7)
print()

odds = []
evens = []
for item in my_list3:
    if (item / 2).is_integer():
        evens.append(item)
    else:
        odds.appends(item)
print("Odds - ", odds)
print("Evens - ", evens)
print()

odds = []
evens = []
for item in my_list4:
    if (item / 2).is_integer():
        evens.append(item)
    else:
        odds.append(item)
print("Odds - ", odds)
print("Evens - ", evens)
print()

odds = []
evens = []
for item in my_list5:
    if (item / 2).is_integer():
        evens.append(item)
    else:
        odds.append(item)
print("Odds - ", odds)
print("Evens - ", evens)







