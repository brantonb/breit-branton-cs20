# Zach & Branton's Asylum Adventure

import time

done = False
power = False
room_list = []

# intro

print("--- DATABOOK #001 ----\n")
time.sleep(1)

print("Analyzing data entry....")
time.sleep(1)

print("Access: GRANTED\n")
time.sleep(1)

print("You are a paranormal investigator who decided to venture off into one of the most insane asylums recorded.")
time.sleep(1)

print("It is a stormy night on June 16, 1994. After an hours drive, you slowly approach the hospital.")
time.sleep(1)

print("As your car slowly crawls towards the front of the hospital, you realize the rusted gates are left wide open.\n")
time.sleep(1)

decision = input("Do you go through the gates? ")
if decision.upper() == "YES":
    print("As you enter the courtyard, the gates slam behind you - locking you in.")
    print("You can type Q anytime to quit.")

elif decision.upper() == "NO":
    print("\nAs you begin to back out from the hospital, you hear a loud noise coming from your tire.")
    print("You quickly exit your vehicle to see that your tire has been penetrated with a shard of glass.")
    print("You decide to enter the courtyard in hopes of finding tools to patch the hole in your tire.")
    print("\nAs you enter the courtyard, the gates slam behind you - locking you in.")
else:
    print("\nYou decide to into the courtyard anyway and the gate slams shut behind you, locking you in.")
    print("You can type Q anytime to quit.")

# index 1
room = ["\nYou are in the courtyard and the front door is locked. You see a window to the South.", None, None, 1, None]
room_list.append(room)

# index 2
room = ["\nYou have entered the abandoned dormitory of the North Wing of the hospital. There is one door"
        " out to the West.", 0, None, None, 2]
room_list.append(room)

# index 3
room = ["\nYou are in the North Hallway; the cafeteria is West, the dormitory is East, and"
        " the hallway continues South.", None, 1, 5, 3, ]
room_list.append(room)

# index 4
room = ["\nYou are in the Cafeteria. The North Hallway is to the East and the"
        " Operation Room is to the South.", None, 2, 4, None]
room_list.append(room)

# index 5
room = ["\nYou are in the Operation Room. The Cafeteria is"
        " North and the South Hallway is to the East.", 3, 5, None, None]
room_list.append(room)

# index 6
room = ["\nYou are in the South Hallway. To the West is the Operation Room. The hallway"
        " continues North and the Warden's Office is to the East.", 2, 6, None, 4]
room_list.append(room)

# index 7
room = ["\nYou have made it into the Wardens Office."
        " To the South is a door that seems to lead to the basement of the facility.", None, None, 7, 5]
room_list.append(room)

# index 8
room = ["\nYou have entered the basement. The exit is back North, but you can see a"
        " flickering, ominous light in the South direction.", 6, None, 8, None]
room_list.append(room)

# Index 9
room = ["\nYou are in the Generator Room. You have turned on the power."
        " Congratulations, you have completed the game!", 7, None, None, None]
room_list.append(room)

next_room = 0
current_room = 0

# main loop for the rooms
while not done:
    if room_list[current_room][0] == "\nYou are in the Generator Room. You have turned on the power." \
                                     " Congratulations, you have completed the game!":
        print("\nYou are in the Generator Room and you have turned on the power. In doing so, the front door"
              "became unlocked and you seized the moment to escape the twisted building."
              " Congratulations, you have completed the game!")
        done = True
        break
    print(room_list[current_room][0])

    direction = input("What direction?(N, W, S, E)")

    game_over = input("Press Q to quit, Enter to continue.")
    if game_over.lower() == "q":
        done = True

    # north direction
    if direction.upper() == "N" or direction.upper() == "NORTH":
        if room_list[current_room][1] is None:
            print("You can't go that way.")
        else:
            current_room = room_list[current_room][1]

    # east direction
    if direction.upper() == "E" or direction.upper() == "EAST":
        if room_list[current_room][2] is None:
            print("You can't go that way.")
        else:
            current_room = room_list[current_room][2]

    # south direction
    if direction.upper() == "S" or direction.upper() == "SOUTH":
        if room_list[current_room][3] is None:
            print("You can't go that way.")
        else:
            current_room = room_list[current_room][3]

    # west direction
    if direction.upper() == "W" or direction.upper() == "WEST":
        if room_list[current_room][4] is None:
            print("You can't go that way.")
        else:
            current_room = room_list[current_room][4]
