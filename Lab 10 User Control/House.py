"""
Example for turning a drawing into a function
House by Ben Camplin
"""
 
import pygame
 
# Define some colors
BLACK = (   0,   0,   0)
WHITE = ( 255, 255, 255)
GREEN = (   0, 255,   0)
RED = ( 255,   0,   0)
BLUE = (   0,   0, 255)
TURQUOISE = ( 24, 201, 178)
ORANGE = ( 255, 136, 0)
PURPLE = ( 173, 0, 204)
GREY = ( 222, 222, 222)
YELLOW = ( 250, 250, 0)
LIGHTBLUE = (145, 228, 255)
DARKGREEN = (0, 156, 60)
 
pygame.init()
 
# Set the width and height of the screen [width, height]
size = (700, 500)
screen = pygame.display.set_mode(size)
 
pygame.display.set_caption("My Game")
 
# Loop until the user clicks the close button.
done = False
 
# Used to manage how fast the screen updates
clock = pygame.time.Clock()
 
# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
 
    # --- Game logic should go here

    # --- Drawing code should go here
 
    # First, clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.
     def draw_house(x, y):
        screen.fill(WHITE)
        # The house
        pygame.draw.rect(screen, ORANGE, [100, 400, 101, -100], 0)
        # roof
        pygame.draw.polygon(screen, RED, [[150,200], [100,298], [200,298]])
        # Window
        pygame.draw.rect(screen, WHITE, [110, 335, 20 , -20])
        # Door
        pygame.draw.rect(screen, BLACK, [150, 400, 20, -50])

        # --- Go ahead and update the screen with what we've drawn.
        pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)
 
# Close the window and quit.
# If you forget this line, the program will 'hang'
# on exit if running from IDLE.
pygame.quit()