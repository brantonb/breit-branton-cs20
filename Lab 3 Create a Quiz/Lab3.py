# score variable
score = 0

# question 1 (menu answer)
hours = input("1.how many hours should branton have on the computer a day to stay healthy\nA = 1 hour\nB = 4 hours\nC = 7 hours\nD = 24 hours\n")
if hours.upper() == "A":
    print("WHAT DO YOU MEAN. THAT'S NOT EVEN ENOUGH TIME FOR HIM TO STAY ALIVE!!!")
elif hours.upper() == "B":
    print("you think 4 HOURS IS HEALTHY! NO!")
elif hours.upper() == "C":
    print("better than A and B but still gaming needs more than that.")
elif hours.upper() == "D":
    print("YES!!! YOU GOT IT!\nwell 24 still is not as good as a lifetime but whatever, you get a point.")
    score += 1
elif hours.upper() == "E":
    print("this is the secret lucky answer and it is a lifetime so you will win the lottery! you get a bonus point!!!")
    score += 2
else:
    print("i dont know what you answered, but it will be how long you still have to live so you better hope your number is high")

# question 2 (text answer)
smartestb = input("\n2.what is the name of the smartest Breit boy?\n")
if smartestb.upper() == "BRANTON":
    print("YES that is correct.")
    score += 1
elif smartestb.upper() == "BRAD" or smartestb.upper() == "BRADLEY":
    print("well i guess that is correct. but you should have said branton so you only get half a mark.")
    score += 0.5
elif smartestb.upper() == "BENJAMIN" or  smartestb.upper() == "BEN":
    print("really? he's the oldest child that doesn't make him the smartest.")
elif smartestb.upper() == "BRADEN":
    print("i guess he looks most like a geek. but that's not true, its still me.")
else:
    print("well the answer is branton")

# question 3 (number answer)
pro = float(input("\n3.how many programmers does it take to screw in a light bulb?\n"))
if pro == 0:
    print("correct. that's a hardware issue, let the engineers do it.")
    score += 1
else:
    print("well no it takes 0 because that's the engineers job")

# question 4 (trick question)
bell = input("4.what time does the bell ring for assembly/mass at leboldus (this seems to be a TRICKy question)\nA = 11:45\nB = 11:35\nC = 11:30\nD = ???\n")
if bell.upper() == "A" or bell.upper() == "B" or bell.upper() == "C":
    print("do you even go to Leboldus you spy, they don't ring the bell they call us down.")
else:
    print("whatever it was: correct, this was a trick question because they don't ring the bell they call us down.")
    score += 1

# print final score and percentage
    print("you scored", score)
    score = score / 4*100
    print("%", score, "")
