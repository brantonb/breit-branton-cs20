# Import
import random
import time
# Adding in variables
done = False
fails = False
failt = False
win = False
finishq = False
miles = 0
stamina = 20
storm = 10
matches = 3
turn = 0

# Printing general information
print("Welcome to BELOW ZERO.")
time.sleep(1)
print("You are a survivalist that is trying to escape the frost storm.")
time.sleep(2)
print("The storm is 10 miles behind you and you need to GET TO DA CHOPPA to escape.")
time.sleep(3)
print("You have 3 matches to start a fire to warm up and gain stamina.")
time.sleep(2)
print("Your stamina is effected by your speed and you start with 20 stamina.")
time.sleep(2)
print("if you need more stamina you can rest but use at your own risk the storm will be coming")
time.sleep(3)
print("Can you travel 150 miles and escape the storm?")
time.sleep(1)

# Boolean variable to control winning and losing
while not done:
    if storm <= 0:
        fails = True
        done = True
    if miles >= 150:
        done = True
        win = True
    ranEvent = random.randrange(1, 11)
    shackR = random.randrange(1, 3)
# shack event
    if ranEvent == 2 and turn == (2, 4) or ranEvent == 2 and turn == (7, 10):
        shack = input("(Event): You found an abandoned shack, would you like to stay?\n(Y/N)")
        if shack.upper() == "Y":
            if shackR == (1, 3):
                print("You stayed and got +10 sta and found 1 match.")
                storm += 10
                stamina += 10
                matches += 1

                if shackR == 3:
                    print("You stayed and rested but there was nothing else there")
                    storm += 10
                    stamina += 5
            elif shack.upper() == "N":
                print("You passed by the shack.")


# Action options
    print("\nA - Use match for fire +5 sta.")
    print("B - Ahead moderate speed")
    print("C - Ahead full speed")
    print("D - rest")
    print("E - Status check")
    print("F - Quit game")
    time.sleep(1)

# warnings
    if storm <= 10 and turn >= 1:
        print("\n!Better hurry up the storm is close!")
    if stamina <= 5:
        print("\n!Stamina low!")
    if matches == 1:
        print("\n!Only one match left!")
    choice = input()

# random events
    if ranEvent == 3 and turn >= 6:
        print("(Event): You found a match.")
        matches += 1
    elif ranEvent == (5, 7) and turn >= 1:
        print("(Event): The storm sped up for an hour and got 10 miles closer, better hurry.")
        storm += 10
    elif ranEvent == (7, 11) and turn >= 1:
        print("(event): you fell in the snow lost a match and -5 sta")
        matches -= 1
        stamina -= 5


# Use match choice
    if choice.upper() == "A" and stamina >= 25:
        print("max stamina (you wasted a match you fool)")
        matches -= 1
        time.sleep(2)
    elif choice.upper() == "A" and stamina < 25 and matches > 0:
            print("+5 sta")
            matches -= 1
            storm -= 5
            turn += 1
            stamina += 5
            time.sleep(1)


# Moderate speed choice
    elif choice.upper() == "B" and stamina >= 5:
        print("You went moderate speed.\n")
        miles += 10
        stamina -= 5
        storm += 5
        turn += 1
        time.sleep(1)

# Full speed choice
    elif choice.upper() == "C" and stamina >= 10:
        print("You went full speed.\n")
        miles += 15
        stamina -= 10
        storm += 5
        turn += 1
        time.sleep(1)

# rest
    elif choice.upper() == "D":
        print("you rested (+5 sta)")
        stamina += 5
        storm += 10
        turn += 1
        time.sleep(1)

# Stat check choice
    elif choice.upper() == "E":
        print("Stats\nMiles: ", miles)
        print("Distance from storm: ", storm)
        print("Stamina: ", stamina,)
        print("matches: ", matches, "\n")
        storm -= 5
        turn += 1
        time.sleep(3)

# Quit game
    elif choice.upper() == "F":
        finish = input("Are you sure you want to quit? you have so much to live for.")
        if finish.upper() == "YES":
            finishq = True
            done = True
        elif finish.upper() == "NO":
            print("That's the way to be.")
        else:
            print("Well you get to play.")

# Limiters
    elif choice.upper() == "C" and stamina < 10:
        print("You don't have enough stamina.\n")
        time.sleep(1)
    elif choice.upper() == "B" and stamina < 5:
        print("You don't have enough stamina.\n")
        time.sleep(1)
    elif choice.upper() == "A" and matches == 0:
        print("your out of matches")
        time.sleep(1)

# Win and lose/quit if statements
if done and win:
    print("You escaped the frost storm, good job!")
    print("The storm was", storm, "miles away.")
elif done and fails:
    print("The storm caught up with you and you froze to death.\nGAME OVER")
elif done and finishq:
    print("Well fine then\nYOU LOSE")
