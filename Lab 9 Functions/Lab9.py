import random

# defining the function to take the parameters and return the smallest
def min3(a, b, c):
    if a <= b and a <= c:
        return a
    elif b <= a and b <= c:
        return b
    elif c <= a and c <= b:
        return c


# sample code to print
print(min3(4, 7, 5))
print(min3(4, 5, 5))
print(min3(4, 4, 4))
print(min3(-2, -6, -100))
print(min3("Z", "B", "A"))
print()


# defining the box function
def box(y, x):
    for row in range(y):
        for r in range(x):
            print("*", end=" ")
        print()


# sample code to print
box(7, 5)  # Print a box 7 high, 5 across
print()   # Blank line
box(3, 2)  # Print a box 3 high, 2 across
print()   # Blank line
box(3, 10)  # Print a box 3 high, 10 across
print()


# defining find
def find(lis, key):
    for i in range(len(my_list)):
        if lis[i] == key:
            print("found", key, "in position", i)


# sample code to print
my_list = [36, 31, 79, 96, 36, 91, 77, 33, 19, 3, 34, 12, 70, 12, 54, 98, 86, 11, 17, 17]
find(my_list, 12)
find(my_list, 91)
find(my_list, 80)
print()


# define create list
def create_list(size):
    ran_list = []
    for i in range(size):
        ran_list.append(random.randrange(0, 7))
    return ran_list


my_list = create_list(5)
print(my_list)


# def count list
def count_list(li, n):
    v = 0
    for i in range(len(li)):
        if li[i] == n:
            v += 1
    return v


# sample code to print
count = count_list([1,2,3,3,3,4,2,1],3)
print(count)


# define average
def average_list(lis):
    a = 0
    for i in range(len(lis)):
        a += lis[i]
    return a / len(lis)


# sample code to print
avg = average_list([1,2, 2, 6, 1, 3])
print(avg)

# create list
my_list = create_list(10000)

# print how many times the 1-6 shows up
print("1 showed up", count_list(my_list, 1), "times")
print("2 showed up", count_list(my_list, 2), "times")
print("3 showed up", count_list(my_list, 3), "times")
print("4 showed up", count_list(my_list, 4), "times")
print("5 showed up", count_list(my_list, 5), "times")
print("6 showed up", count_list(my_list, 6), "times")

# average of the list
avg = average_list(my_list)
print(avg)
