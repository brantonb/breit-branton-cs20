# Branton
# lab1 calculators
# computer science 20

# miles driven per gallons used calculator
# get value for milesDriven and gallonsUsed
milesDriven = float(input("How many miles driven: "))
gallonsUsed = float(input("How many gallons were used: "))

# print the equation
print("Your miles driven (", milesDriven, ") divided by your gallons used (", gallonsUsed, ") is your miles per gallon")
# print answer
print("Miles per gallon is equal to", milesDriven / gallonsUsed)


# fahrenheit to celsius calculator
# get value in fahrenheit
fahrenheit = float(input("\nEnter a temperature in fahrenheit to get it in celsius: "))

# calculate the temperature and print
celsius = (fahrenheit - 32) * 5/9
print("Temperature in celsius:", celsius)


# calculator for the area of a trapezoid
# get values
print("Calculating the area of a trapezoid")
height = float(input("\nWhat is the height of the trapezoid: "))
lengthBB = float(input("What is the bottom base of the trapezoid: "))
lengthTB = float(input("What is the top base of the trapezoid: "))

# print the formula, equation and answer
area = 0.5 * (lengthTB + lengthBB) * height
print("Area = 1/2 x ( TopBase + BottomBase ) x Height")
print("Area = 1/2 (", lengthTB, "+", lengthBB, ") x ", height, "=", area, )

# volume of a cylinder calculator
# get values
print("\nCalculating the volume of a cylinder")
radius = float(input("What is the radius of the cylinder"))
cylinderHeight = float(input("What is the height of the cylinder"))

# print the formula, equation and answer
volume = 3.14 * (radius**2) * cylinderHeight
print("Volume = 3.14 x Radius**2 x Height")
print("Volume = 3.14 x Radius (", radius, ") **2 x Height (", cylinderHeight, ") =", volume)
