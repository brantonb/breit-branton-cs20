# import
import pygame

# defining colors
BLACK = (0,   0,   0)
WHITE = (255, 255, 255)
GREEN = (0, 225,   0)
RED = (255,   0,   0)
BLUE = (0,   0, 255)
TEAL = (0, 255, 246)
PINK = (233, 0, 255)
KIRB = (255, 114, 152)
FOOT = (214, 47, 49)
SKY = (83, 160, 232)
CHEEK = (239, 92, 134)
GRASS = (66, 168, 58)
TREE = (122, 85, 33)
BTREE = (150, 99, 27)
MOUTH = (252, 27, 61)
TONGUE = (219, 0, 40)
ARM = (255, 89, 133)
POT = (255, 238, 0)
TPOT = (226, 211, 0)
INMOTH = (196, 155, 155)
TOP = (153, 53, 53)

aY = 416
delta_y = 0.15

pygame.init()
size = (1280, 720)
screen = pygame.display.set_mode(size)
# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()
linesSaved = []
# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():  # User did something
        if event.type == pygame.QUIT:  # If user clicked close
            done = True  # Flag that we are done so we exit this loop

    # --- Game logic should go here
    tx_offset = 0
    gx_offset = 0
    # defining kirb as a function

    def kirb():
        pygame.draw.ellipse(screen, FOOT, [240, 561, 200, 100])
        pygame.draw.ellipse(screen, KIRB, [90, aY - 135, 385, 345])
        pygame.draw.ellipse(screen, FOOT, [105, 571, 200, 100])
        pygame.draw.ellipse(screen, CHEEK, [200, 435, 90, 50])
        pygame.draw.ellipse(screen, CHEEK, [400, 425, 65, 40])
        pygame.draw.ellipse(screen, MOUTH, [295, 450, 115, 65])
        pygame.draw.ellipse(screen, TONGUE, [320, 485, 70, 30])
        pygame.draw.ellipse(screen, BLACK, [275, 350, 40, 70])
        pygame.draw.ellipse(screen, BLACK, [360, 350, 40, 70])
        pygame.draw.ellipse(screen, WHITE, [370, 360, 20, 35])
        pygame.draw.ellipse(screen, WHITE, [285, 360, 20, 35])
        pygame.draw.ellipse(screen, ARM, [90, aY, 120, 175])


    # --- Drawing code should go here

    # First, clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.
    screen.fill(BTREE)
    # ground/grass & trees
    while tx_offset < 1400:
        pygame.draw.line(screen, TREE, [0 + tx_offset, 0], [0 + tx_offset, 720, ], 53)
        tx_offset += 80
    pygame.draw.rect(screen, GREEN, [0, 650, 1280, 200])
    while gx_offset < 1400:
        pygame.draw.line(screen, GRASS, [0 + gx_offset, 720], [0 + gx_offset, 635], 2)
        gx_offset += 2.5

    # making kirby bounce (idle animation in game)
    if aY >= 420 or aY <= 415:
        delta_y = - delta_y
    aY += delta_y
    kirb()

    # plant
    pygame.draw.ellipse(screen, POT, [920, 625, 125, 35])
    pygame.draw.polygon(screen, POT, ((920, 642), (910, 546), (1047, 550), (1042, 641), (920, 642)))
    pygame.draw.ellipse(screen, TPOT, [902, 525, 160, 50])
    pygame.draw.polygon(screen, TPOT, ((902, 554), (900, 512),
                                       (909, 517), (916, 520), (923, 522), (929, 523), (935, 526),
                                       (942, 529), (957, 531), (969, 532), (980, 532), (991, 532),
                                       (1006, 529), (1018, 526), (1029, 523), (1037, 519), (1051, 514),
                                       (1059, 511), (1060, 510), (1061, 555)))
    pygame.draw.ellipse(screen, TREE, [923, 530, 115, 20])
    pygame.draw.polygon(screen, TPOT, ((917, 533), (931, 539),
                                       (940, 541), (949, 541), (963, 542), (976, 542), (990, 542),
                                       (1009, 542), (1025, 541), (1032, 539), (1039, 535), (1043, 539),
                                       (1043, 545), (1028, 547), (1007, 551), (988, 552), (966, 552),
                                       (944, 551), (924, 547), (918, 543), (916, 533),))
    pygame.draw.polygon(screen, TPOT, ((899, 512), (903, 509),
                                       (908, 507), (917, 504), (932, 502), (945, 500),
                                       (970, 498), (992, 497), (1014, 497), (1031, 500),
                                       (1044, 503), (1050, 505), (1058, 509), (1059, 510),
                                       (1058, 528), (1034, 528), (1001, 529), (970, 526),
                                       (956, 529), (929, 532), (917, 529), (905, 519), (900, 512),))
    pygame.draw.polygon(screen, GRASS, (
        (980, 537), (975, 517), (971, 495), (976, 484), (990, 473),
        (1003, 448), (1006, 408), (993, 355), (981, 332), (957, 331), (928, 339),
        (920, 345), (929, 339), (943, 335), (957, 334), (976, 333), (983, 337),
        (988, 354), (996, 373), (1000, 398), (1002, 421), (1004, 438), (999, 461),
        (988, 475), (975, 485), (969, 503), (976, 521), (979, 533)), 20)
    pygame.draw.polygon(screen, GRASS, (
        (979, 477), (966, 467), (964, 455), (954, 458), (940, 448),
        (926, 455), (912, 441), (903, 448), (898, 442), (894, 450),
        (884, 444), (880, 450), (869, 447), (872, 458), (868, 460),
        (878, 464), (871, 471), (880, 474), (882, 479), (876, 493),
        (894, 492), (908, 495), (913, 511), (931, 505), (938, 513),
        (943, 495), (951, 491), (966, 490), (980, 478)))
    pygame.draw.polygon(screen, GRASS, (
        (981, 491), (995, 493), (1005, 497), (1012, 505), (1024, 485),
        (1035, 480), (1046, 489), (1050, 468), (1058, 463), (1072, 472),
        (1076, 450), (1094, 449), (1091, 442), (1101, 442), (1099, 438),
        (1102, 437), (1095, 435), (1089, 432), (1089, 429), (1079, 431),
        (1073, 431), (1066, 428), (1056, 434), (1046, 433), (1039, 442),
        (1032, 442), (1033, 450), (1027, 456), (1022, 456), (1019, 464),
        (1013, 465), (1012, 474), (1005, 476), (996, 474), (988, 472),
        (981, 491)))
    pygame.draw.polygon(screen, GRASS, (
        (979, 323), (985, 325), (990, 329), (995, 338), (997, 349), (971, 325)))
    pygame.draw.polygon(screen, GRASS, (
        (964, 342), (974, 347), (982, 355), (973, 331)))
    pygame.draw.polygon(screen, GRASS, (
        (960, 503), (962, 518), (963, 524), (961, 529), (957, 536),
        (958, 538), (974, 540), (990, 540), (997, 537), (996, 532),
        (990, 528), (985, 520), (974, 506)))
    pygame.draw.ellipse(screen, TONGUE, [778, 250, 180, 190])
    pygame.draw.ellipse(screen, WHITE, [850, 428, 40, 12])
    pygame.draw.polygon(screen, WHITE, (
        (843, 442), (859, 432), (881, 419), (906, 400), (926, 381),
        (936, 363), (940, 348), (940, 336), (938, 327), (929, 315),
        (915, 308), (886, 297), (847, 286), (801, 277), (767, 273),
        (745, 274), (728, 279), (719, 290), (716, 302), (720, 315),
        (728, 326), (747, 342), (770, 357), (770, 367), (781, 364),
        (785, 368), (776, 378), (763, 398), (755, 421), (758, 444),
        (775, 454), (809, 452), (844, 442)))
    pygame.draw.polygon(screen, INMOTH, (
        (909, 341), (914, 345), (914, 351), (901, 368), (858, 397),
        (803, 424), (774, 428), (766, 421), (771, 403), (787, 378),
        (796, 370), (791, 356), (761, 340), (738, 323), (735, 310),
        (738, 301), (747, 298), (770, 297), (796, 304), (910, 342)))
    pygame.draw.polygon(screen, TONGUE, (
        (906, 362), (892, 361), (873, 365), (856, 373), (830, 378),
        (819, 385), (815, 391), (814, 396), (815, 402), (819, 405),
        (824, 408), (836, 409), (873, 388), (893, 375), (906, 363)))
    pygame.draw.polygon(screen, TOP, (
        (787, 379), (832, 378), (861, 371), (877, 364), (906, 362),
        (914, 352), (914, 345), (906, 340), (769, 296), (743, 299),
        (735, 304), (734, 317), (736, 323), (749, 333), (779, 352),
        (790, 357), (794, 365), (792, 372), (788, 379)))
    pygame.draw.polygon(screen, WHITE, (
        (772, 336), (777, 337), (780, 338), (784, 341), (788, 346),
        (790, 349), (790, 352), (779, 364), (766, 373), (769, 354), (772, 337)))
    pygame.draw.polygon(screen, WHITE, (
        (738, 290), (745, 325), (761, 291), (782, 295), (786, 330),
        (805, 299), (841, 316), (844, 350), (868, 324), (761, 277)))
    pygame.draw.polygon(screen, WHITE, (
        (884, 385), (853, 366), (845, 365), (854, 388),
        (861, 404), (826, 419), (808, 393), (806, 407), (794, 392),
        (797, 429), (781, 407), (780, 413), (781, 421), (784, 433),
        (820, 436), (885, 402)))
    pygame.draw.polygon(screen, WHITE, (
        (804, 277), (817, 267), (822, 266), (829, 268), (830, 272),
        (826, 279), (821, 282), (803, 280)))
    pygame.draw.polygon(screen, WHITE, (
        (887, 253), (879, 253), (873, 255), (868, 259), (867, 263),
        (869, 267), (875, 270), (889, 278), (905, 283), (915, 283),
        (920, 282), (922, 278), (921, 272), (914, 265), (902, 258), (884, 253)))
    pygame.draw.polygon(screen, WHITE, (
        (940, 397), (937, 403), (931, 410), (922, 415), (917, 415),
        (915, 414), (914, 412), (915, 408), (917, 405), (926, 396),
        (932, 391), (936, 390), (940, 391), (940, 394), (938, 399)))

    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)
